FROM cern/cc7-base

# https://grafanarel.s3.amazonaws.com/builds/grafana-3.1.0-1468321182.x86_64.rpm
ARG GRAFANA_RPM_URL
USER root
EXPOSE 3000

ADD root /
RUN yum -y install ${GRAFANA_RPM_URL} \
    libXcomposite libXdamage libXtst cups libXScrnSaver pango atk adwaita-cursor-theme \
    adwaita-icon-theme at at-spi2-atk at-spi2-core cairo-gobject colord-libs dconf \
    desktop-file-utils ed emacs-filesystem gdk-pixbuf2 glib-networking gnutls \
    gsettings-desktop-schemas gtk-update-icon-cache gtk3 hicolor-icon-theme \
    jasper-libs json-glib libappindicator-gtk3 libdbusmenu libdbusmenu-gtk3 libepoxy \
    liberation-fonts liberation-narrow-fonts liberation-sans-fonts liberation-serif-fonts \
    libgusb libindicator-gtk3 libmodman libproxy libsoup libwayland-cursor libwayland-egl \
    libxkbcommon m4 mailx nettle patch psmisc redhat-lsb-core redhat-lsb-submod-security \
    rest spax time trousers xdg-utils xkeyboard-config alsa-lib \
    && yum clean all

COPY run.sh /usr/share/grafana/
RUN /usr/bin/fix-permissions /usr/share/grafana \
    && /usr/bin/fix-permissions /etc/grafana \
    && /usr/bin/fix-permissions /var/lib/grafana \
    && /usr/bin/fix-permissions /var/log/grafana

WORKDIR /usr/share/grafana

VOLUME ['/var/lib/grafana']

ENTRYPOINT ["./run.sh"]
