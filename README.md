Overview
========
This project provides a Grafana Docker Image ready to be run in OpenShift. A template
with all the necessary components to launch an instance with Single Sign-On configured
is also provided

Deployment
==========
The Docker Image is built using gitlab-ci and pushed it to an ImageStream with name `grafana` 
in the Openshift registry in the `openshift` namespace. The template is also deployed
in the `openshift` namespace.

Changes in the image and/or the template are fully handled by GitLab-CI:
* A push to a custom branch will build and deploy a new image with tag `latest` and
also update the template in the testing cluster (`openshift-dev.cern.ch`), for development
purposes.
* When the changes are ready for production, merge to master. This will build and deploy the
new image with tag `latest` in the production cluster (`openshift.cern.ch`). The deployment
of the template requires a manual trigger to ensure it doesn't happen by mistake.
* After all this, the image is ready to be tagged as `stable`. As this is the tag all instances
use by default, changing the tag to a new image will trigger a re-deploy of all existing apps.
This step is also handled by a manual trigger (both for production and development clusters)